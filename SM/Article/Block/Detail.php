<?php

namespace SM\Article\Block;

use Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \SM\Article\Model\ResourceModel\Article\CollectionFactory;
use Magento\Framework\App\Request\Http;



class Detail extends Template
{
    public $_collectionFactory;

    public $_request;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        Http $request
    )
    {
        $this->_collectionFactory = $collectionFactory;
        $this->_request = $request;
        parent::__construct($context);
    }

    public function getArticleDetail()
    {
        $idArticle = $this->_request->getParams();
        return $this->_collectionFactory->create()->addFieldToFilter(['article_id'], [$idArticle['id']])->getData();
    }

}
