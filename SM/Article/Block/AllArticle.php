<?php

namespace SM\Article\Block;

use Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \SM\Article\Model\ResourceModel\Article\CollectionFactory;


class AllArticle extends Template
{
    public $_collectionFactory;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory
    )
    {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function article()
    {
        $a = $this->_collectionFactory->create();
        return $a->getData();
    }
}
