<?php

namespace SM\Article\Model\ResourceModel\Article;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\SM\Article\Model\Article::class, \SM\Article\Model\ResourceModel\Article::class);
    }
}
