<?php

namespace SM\Article\Model\ResourceModel;

class Article extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sm_article', 'article_id');
    }
}