<?php

namespace SM\Article\Model;

class Article extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize banner model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\SM\Article\Model\ResourceModel\Article::class);
    }
}